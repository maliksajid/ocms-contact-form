<?php namespace Sajid\Contactform;

use Backend;
use System\Classes\PluginBase;

/**
 * contactform Plugin Information File
 */
class Plugin extends PluginBase
{
    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails()
    {
        return [
            'name'        => 'contactform',
            'description' => 'A simple contact form plug-in',
            'author'      => 'sajid',
            'icon'        => 'icon-leaf'
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Boot method, called right before the request route.
     *
     * @return array
     */
    public function boot()
    {

    }

    /**
     * Registers any front-end components implemented in this plugin.
     *
     * @return array
     */
    public function registerComponents()
    {
        return [
            'Sajid\Contactform\Components\ContactForm' => 'contactForm',
        ];
    }

    /**
     * Registers any back-end permissions used by this plugin.
     *
     * @return array
     */
    public function registerPermissions()
    {
        return []; // Remove this line to activate

        return [
            'sajid.contactform.some_permission' => [
                'tab' => 'contactform',
                'label' => 'Some permission'
            ],
        ];
    }

    /**
     * Registers back-end navigation items for this plugin.
     *
     * @return array
     */
    public function registerNavigation()
    {
        return [
            'contactform' => [
                'label'       => 'contactform',
                'url'         => Backend::url('sajid/contactform/mycontroller'),
                'icon'        => 'icon-leaf',
                'permissions' => ['sajid.contactform.*'],
                'order'       => 500,
            ],
        ];
    }
}

<?php namespace Sajid\Contactform\Components;

use Cms\Classes\ComponentBase;

class ContactForm extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name' => 'Contact Form',
            'description' => 'A simple contact form',
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onSend()
    {
        // Get request data
        $data = \Input::only([
            'first_name',
            'last_name',
            'email',
            'content',
        ]);

        // Validate request
        $this->validate($data);

        // Send email
        $receiver = 'admin@gmail.com';

        \Mail::send('sajid.contact::contact', $data, function ($message) use ($receiver) {
            $message->to($receiver);
        });
    }

    protected function validate(array $data)
    {
        // Validate request
        $rules = [
            'first_name' => 'required|min:3|max:255',
            'last_name' => 'required|min:3|max:255',
            'email' => 'required|email',
            'content' => 'required',
        ];

        $validator = \Validator::make($data, $rules);

        if ($validator->fails()) {
            throw new ValidationException($validator);
        }
    }
}
